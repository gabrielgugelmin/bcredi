document.addEventListener('DOMContentLoaded', function() {
	clickAttached = false;

	_loadContent();
	
	window.addEventListener('resize', function() {
		_attachClicks();
	});
}, false);

var _loadContent = function () {
	var request = new XMLHttpRequest();

	request.overrideMimeType("application/json");
	request.open('GET', 'assets/json/fazenda.json', true);	

	request.onload = function() {
		if (request.status >= 200 && request.status < 400) {
			var result = JSON.parse(request.responseText);
		
			var template = document.querySelector('#test').innerHTML;
			var output = document.querySelector('#output');
			
			var r = _consolidate(result);
			var result = Mustache.to_html(template, JSON.parse(r));
			
			output.innerHTML = result;

			_attachClicks();
			
		} else {
			// We reached our target server, but it returned an error
		}
	};

	request.onerror = function() {
		// There was a connection error of some sort
	};

	request.send();
}

var _consolidate = function (result) {
	var r = result.data.map(function (element) {
		// valida texto e trata o encode
		if (element.description) element.description = he.decode(element.description);

		// cria o percentual de votos positivos
		element.positivePercentage = _getPercentage(element.positive, element.negative);
		
		// cria o percentual de votos negativos
		element.negativePercentage = _getPercentage(element.negative,  element.positive);

		return element;
	});

	// ordena o JSON do maior percentual para o menor
	r.sort(function(a, b) {
    return a.positivePercentage < b.positivePercentage;
	});

	// inclui posição ranking e valida percentuais
	r.map(function (element, index) {
		element.rank = index + 1;

		if (element.positivePercentage === null) {
			element.positivePercentage = '-';
		}
		if (element.negativePercentage === null) {
			element.negativePercentage = '-';
		}

		return element;
	});
	
	return JSON.stringify(r);
}

var _getPercentage = function (a, b) {
	if (a && b) {
		var total = parseInt(a, 10) + parseInt(b, 10);

		return ((a / total) * 100).toFixed(0);
	} else {
		return null;
	}
}

var _attachClicks = function () {
	if (window.innerWidth <= 991 && !clickAttached) {
		var cards = document.querySelectorAll('.card__item');

		Array.prototype.forEach.call(cards, function (element) {
			element.addEventListener('click', function (event) {
				_toggleVotes(element);
			});
		});

		clickAttached = true;
	}
}

var _toggleVotes = function (element) {
	var isVoteActive = element.classList.contains('card__item--open');
	return (isVoteActive) ? element.classList.remove('card__item--open') : element.classList.add('card__item--open');
}